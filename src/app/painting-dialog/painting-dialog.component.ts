import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { PaintingService } from '../services/painting.service';

@Component({
  selector: 'app-painting-dialog',
  templateUrl: './painting-dialog.component.html',
  styleUrls: ['./painting-dialog.component.scss']
})
export class PaintingDialogComponent implements OnInit {

  constructor(private paintingService: PaintingService,
            @Inject(MAT_DIALOG_DATA) public data:any, 
            private router: Router, 
            public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  modifyPaintings(data: any) {
    this.router.navigate(['/manage'], { state: data});
    this.dialog.closeAll();
  }

  deletePainting(data: any) {
    this.paintingService.deletePainting(data);
    this.dialog.closeAll();
    this.router.navigateByUrl('/paintings');
  }
}
